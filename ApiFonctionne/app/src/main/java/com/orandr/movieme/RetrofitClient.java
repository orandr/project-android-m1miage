package com.orandr.movieme;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    //   https://api.themoviedb.org/3/search/movie?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&query=jumanji&page=1&include_adult=false
    /*   https://api.themoviedb.org/3/search/movie?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&query= */ /*jumanji*/  /*&page=1&include_adult=false*/


    //   https://api.themoviedb.org/3/movie/now_playing?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1
    //   https://api.themoviedb.org/3/movie/popular?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1
    //   https://api.themoviedb.org/3/movie/upcoming?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1

    private static Retrofit retrofit;
//    private static final String BASE_URL = "https://developers.themoviedb.org/3/";
    private static final String BASE_URL = "https://api.themoviedb.org/";
//    private static final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
//    private static final String LANGUAGE = "en-US";

    public static Retrofit getInstance() {
        if( retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient httpClient =  new OkHttpClient.Builder().addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
