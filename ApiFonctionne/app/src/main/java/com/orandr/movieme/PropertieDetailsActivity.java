package com.orandr.movieme;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.Properties;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.orandr.movieme.MainActivity.MOVIE_NAME;

public class PropertieDetailsActivity extends AppCompatActivity {

    private TextView title;
    private TextView id;
    private TextView overview;
    private TextView poster_path;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propertie_details);

        title = findViewById(R.id.movie_title);
        id = findViewById(R.id.movie_id);
        overview = findViewById(R.id.movie_overview);
        poster_path = findViewById(R.id.movie_poster_path);


        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            String name = bundle.getString(MOVIE_NAME);
            fetchMovieData(name);
        }

    }

    private final String BASE_URL = "https://developers.themoviedb.org/3/";
    private final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    private final String LANGUAGE = "en-US";

    private void fetchMovieData(String name) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getNowPlaying(API_KEY,LANGUAGE).enqueue(new Callback<ListCollection>() {
            @Override
            public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                if(response.isSuccessful() && response.body() != null){
                    ListCollection collection = response.body();
                    displayData(collection.getResult().get(0));
                }else {
                    Toast.makeText(getApplicationContext(),getString(R.string.app_error),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ListCollection> call, Throwable t) {

            }
        });
    }

    private void displayData(Properties properties) {
        title.setText(properties.getTitle());
        id.setText(properties.getId());
        overview.setText(properties.getOverview());
        poster_path.setText(properties.getPoster_path());
    }
}
