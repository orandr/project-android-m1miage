package com.orandr.movieme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.Properties;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerAdapter;
    public static final String MOVIE_NAME = "movie_name";
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);

        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Properties properties) {
                Intent intent = new Intent(getApplicationContext(), PropertieDetailsActivity.class);
                intent.putExtra(MOVIE_NAME, properties.getTitle());
                startActivity(intent);
            }
        },getApplicationContext());



        recyclerView.setAdapter(recyclerAdapter);
        fetchMovieData();

    }

    private final String BASE_URL = "https://developers.themoviedb.org/3/";
    private final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    private final String LANGUAGE = "en-US";

    // Attention, getResult 0
    private void fetchMovieData() {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getNowPlaying(API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
//                        Log.i("info1",response.isSuccessful()+"");
                        if( response.isSuccessful() && response.body() != null ) {
                        //Manage Data
//                            Log.i("info2","avant");
                        ListCollection collection = response.body();

//                            Log.i("info3", collection.getResult().get(0)+"");
                        recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(getApplicationContext(), "fectMovieData error, body result null", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }


}
