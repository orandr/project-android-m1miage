package com.orandr.movieme;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.orandr.movieme.model.Properties;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private ArrayList<Properties> propertiesList;
    private OnItemClickListener listener;
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(View view, Properties properties);
    }

    public RecyclerViewAdapter(ArrayList<Properties> propertiesList, OnItemClickListener listener, Context context) {
        this.propertiesList = propertiesList;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return this.propertiesList.size();
    }

    public void addFeatureList(ArrayList<Properties> featureList) {
        this.propertiesList.addAll(featureList);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_propertie, parent, false);
        return new RecyclerViewAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Properties properties = propertiesList.get(position);

        Log.i("prop_id", ""+properties.getId());
        Log.i("prop_title", ""+properties.getTitle());
        Log.i("prop_poster_path", ""+properties.getPoster_path());
        Log.i("prop_release_date", ""+properties.getRelease_date());
        String urlPoster = "https://image.tmdb.org/t/p/original"+properties.getBackdrop_path();
        Log.i("Poster",urlPoster);
        loadImageFromURL(holder,urlPoster);
//        holder.propertiePosterPath.setImageResource(R.drawable.backdrop);
//        holder.propertiePosterPath.setImageURI("https://image.tmdb.org/t/p/original"+properties.getBackdrop_path());
        holder.propertieTitle.setText(""+properties.getTitle());
        holder.propertieVoteAverage.setText(""+properties.getVote_average());
        holder.propertieReleaseDate.setText(""+properties.getRelease_date());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, properties);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View itemView;
        final ImageView propertiePosterPath;
        final TextView propertieTitle;
        final TextView propertieVoteAverage;
        final TextView propertieReleaseDate;
        ViewHolder(View view) {
            super(view);
            itemView = view;
            propertiePosterPath = view.findViewById(R.id.imageView);
            propertieTitle = view.findViewById(R.id.propertie_title);
            propertieVoteAverage = view.findViewById(R.id.propertie_vote_average);
            propertieReleaseDate = view.findViewById(R.id.propertie_release_date);
        }

    }


    private void loadImageFromURL(ViewHolder holder,String url) {
        Picasso.with( context )
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.propertiePosterPath, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }

}
