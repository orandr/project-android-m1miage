package com.orandr.movieme;


import android.util.Log;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.Properties;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FetchMovie {

    private float popularity;
    private int vote_count;
    private boolean video;
    private String poster_path;
    private int id;
    private boolean adult;
    private String backdrop_path;
    private String original_language;
    private String original_title;
    private ArrayList<Integer> genre_ids;
    private String title;
    private float vote_average;
    private String overview;
    private String release_date;

//    "original_title":"Onward"
//    "poster_path":"\/3rQx8kJNfIZnlbILHUbRzK2HOng.jpg"
//    "backdrop_path":"\/xFxk4vnirOtUxpOEWgA1MCRfy6J.jpg"

    //image ==>   https://image.tmdb.org/t/p/original/4U7hpTK0XTQBKT5X60bKmJd05ha.jpg
    //image ==>   https://image.tmdb.org/t/p/original/{poster_path}



    //   https://api.themoviedb.org/3/search/movie?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&query=jumanji&page=1&include_adult=false
    /*   https://api.themoviedb.org/3/search/movie?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&query= */ /*jumanji*/  /*&page=1&include_adult=false*/


    //   https://api.themoviedb.org/3/movie/now_playing?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1
    //   https://api.themoviedb.org/3/movie/popular?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1
    //   https://api.themoviedb.org/3/movie/upcoming?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1

    private final String BASE_URL = "https://developers.themoviedb.org/3/";
    private final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    private final String LANGUAGE = "en-US";

    private ListCollection collection;

    private void displayResultData(Properties result) {
        this.popularity = result.getPopularity();
        this.vote_count = result.getVote_count();
        this.video = result.isVideo();
        this.poster_path = result.getPoster_path();
        this.id = result.getId();
        this.adult = result.isAdult();
        this.backdrop_path = result.getBackdrop_path();
        this.original_language = result.getOriginal_language();
        this.original_title = result.getOriginal_title();
        this.genre_ids = result.getGenre_ids();
        this.title = result.getTitle();
        this.vote_average = result.getVote_average();
        this.overview = result.getOverview();
        this.release_date = result.getRelease_date();

//        System.out.println("poster=>"+this.poster_path+"<=");
        Log.i("poster","=>"+this.poster_path+"<=");
    }


    // Attention, getResult 0
    private void fetchMovieDataNowPlayingPage(Integer num_page) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getNowPlayingPage(API_KEY, LANGUAGE, num_page)
            .enqueue(new Callback<ListCollection>() {
                @Override
                public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                    //if( response.isSuccessful() && response.body() != null ) {*/
                        //Manage Data
                        collection = response.body();
                        Properties p = collection.getResult().get(0);
                        displayResultData(p);
                        Log.d("prop_id", ""+p.getId());
                        Log.d("prop_title", ""+p.getTitle());
                        Log.d("prop_poster_path", ""+p.getPoster_path());
                        Log.d("prop_release_date", ""+p.getRelease_date());


                    /*} else {
                        Toast.makeText(getApplicationContext(), getString(R.string.app_error), Toast.LENGTH_LONG).show();
                    }*/
                }

                @Override
                public void onFailure(Call<ListCollection> call, Throwable t) {
                    //Manage errors
//                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                     }
            });
    }

    // Attention, getResult 0
    private void fetchMovieDataNowPlaying() {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getNowPlaying(API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        //if( response.isSuccessful() && response.body() != null ) {*/
                        //Manage Data
                        collection = response.body();
                        displayResultData(collection.getResult().get(0));
                    /*} else {
                        Toast.makeText(getApplicationContext(), getString(R.string.app_error), Toast.LENGTH_LONG).show();
                    }*/
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }

    // Attention, getResult 0
    private void fetchMovieDataUpcoming() {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getUpcoming(API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        //if( response.isSuccessful() && response.body() != null ) {*/
                        //Manage Data
                        collection = response.body();
                        displayResultData(collection.getResult().get(0));
                    /*} else {
                        Toast.makeText(getApplicationContext(), getString(R.string.app_error), Toast.LENGTH_LONG).show();
                    }*/
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }

    // Attention, getResult 0
    private void fetchMovieDataUpcomingPage(Integer num_page)  {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getUpcomingPage(API_KEY, LANGUAGE, num_page)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        //if( response.isSuccessful() && response.body() != null ) {*/
                        //Manage Data
                        collection = response.body();
                        displayResultData(collection.getResult().get(0));
                    /*} else {
                        Toast.makeText(getApplicationContext(), getString(R.string.app_error), Toast.LENGTH_LONG).show();
                    }*/
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }



}
