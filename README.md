<html>
<body>
<h1>MovieMe</h1>
<h3>1-Pour télécharger l'application , vruillez cliquer <a href="https://drive.google.com/file/d/1p25yeEdTq7gWp7uydGq900moLy4hxeE8/view?usp=sharing">ICI</a></h3>
<h3>2-Vidéo explicative du projet. Pour activer le son veuillez cliquer sur la vidéo</h3>
<br>
<a href="https://drive.google.com/file/d/1pwZVMErADy94WopNlcnApH4SlZWyU0nE/view"><img src="/media/Projet-Android-M1MIAGE-GHAMMAZ-O.gif"/></a>
<h1>Les fonctionnalités de l'application </h1>
<h3>
    <ul>
     <li>Le SplashScreen</li>
     <li>Les 4 liste de film (Top Rated, Upcoming, Now, Popular)</li>
     <li>Les détails des films (Image, Titre, Réalisateur, Overview, ...)</li>
     <li>Les favoris</li>
     <li>La recherche</li>
     <li>L'utilisation de recyclerAdapter, recyclerView</li>
     <li>Un ToolBar</li>
     <li>Une NavigationBar</li>
     <li>La possibilité de voir les films similaires et, bien sûr, d'être rediriger vers celui-ci</li>
     <li>La possibilité de voir les acteurs qui ont participé à la réalisation du film. (Pas de redirection)</li>
     <li>La mise à jour instantané des films en favoris depuis cette activité ou depuis le détail d'un film</li>
     <li>La recherche avec une mise à jour à chaque touche du clavier</li>
     <li>La récupération de l'heure, genre, durée, ...</li>
     <li>L'utilisation de la pagination [voir page 2, 3, ....] pour chaque liste de film  (Top Rated, Upcoming, Now, Popular)</li>
     <li>L'ajout au favoris depuis chaque  liste de film activités  (Top Rated, Upcoming, Now, Popular) directement sur l'image du film (coeur)</li>
    </ul>
</h3>
<br>
<br>
<h4>Réalisé par : ORAND Régis & GHAMMAZ Ayoub</h4>
<h4>LinkedIn : <a href="https://www.linkedin.com/in/r%C3%A9gis-orand-222716140/"> Régis </a> & <a href="https://www.linkedin.com/in/ayoub-ghammaz-3899ba175/"> Ayoub </a> </h4>
<h4>Mail : orand.regis@yahoo.fr - ayoubghammaz1@gmail.com </h4>
</body>
</html>
