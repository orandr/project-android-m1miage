package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListCollection {
    private ArrayList<Properties> result;
    @SerializedName("page")
    private int page;
    @SerializedName("total_results")
    private int total_results;
    @SerializedName("dates")
    private Dates dates;
    @SerializedName("total_pages")
    private int total_pages;

    public ListCollection(ArrayList<Properties> result, int page, int total_results, Dates dates, int total_pages) {
        this.result = result;
        this.page = page;
        this.total_results = total_results;
        this.dates = dates;
        this.total_pages = total_pages;
    }

    public ArrayList<Properties> getResult() {
        return result;
    }

    public int getPage() {
        return page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public Dates getDates() {
        return dates;
    }

    public int getTotal_pages() {
        return total_pages;
    }
}


/*
    {
  "results": [
    {
      "popularity": 215.93,
      "vote_count": 174,
      "video": false,
      "poster_path": "/4U7hpTK0XTQBKT5X60bKmJd05ha.jpg",
      "id": 570670,
      "adult": false,
      "backdrop_path": "/uZMZyvarQuXLRqf3xdpdMqzdtjb.jpg",
      "original_language": "en",
      "original_title": "The Invisible Man",
      "genre_ids": [
        27,
        9648,
        878,
        53
      ],
      "title": "The Invisible Man",
      "vote_average": 7.5,
      "overview": "When Cecilia's abusive ex takes his own life and leaves her his fortune, she suspects his death was a hoax. As a series of coincidences turn lethal, Cecilia works to prove that she is being hunted by someone nobody can see.",
      "release_date": "2020-02-26"
    }
  ],
  "page": 1,
  "total_results": 429,
  "dates": {
    "maximum": "2020-03-26",
    "minimum": "2020-03-03"
  },
  "total_pages": 22
}
 */