package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListNoDateCollection {
    private ArrayList<Properties> result;
    @SerializedName("page")
    private int page;
    @SerializedName("total_results")
    private int total_results;
    @SerializedName("total_pages")
    private int total_pages;

    public ListNoDateCollection(ArrayList<Properties> result, int page, int total_results, int total_pages) {
        this.result = result;
        this.page = page;
        this.total_results = total_results;
        this.total_pages = total_pages;
    }

    public ArrayList<Properties> getResult() {
        return result;
    }

    public int getPage() {
        return page;
    }

    public int getTotal_results() {
        return total_results;
    }


    public int getTotal_pages() {
        return total_pages;
    }
}
