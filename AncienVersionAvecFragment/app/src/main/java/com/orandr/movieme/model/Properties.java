package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Properties {
        private String popularity;
        private int vote_count;
        private boolean video;
        private String poster_path;
        private int id;
        private boolean adult;
        private String backdrop_path;
        private String original_language;
        private String original_title;
        private ArrayList<Integer> genre_ids;
        private String title;
        private int vote_average;
        private String overview;
        private String release_date;

        public Properties(String popularity, int vote_count, boolean video, String poster_path, int id, boolean adult, String backdrop_path, String original_language, String original_title, ArrayList<Integer> genre_ids, String title, int vote_average, String overview, String release_date) {
            this.popularity = popularity;
            this.vote_count = vote_count;
            this.video = video;
            this.poster_path = poster_path;
            this.id = id;
            this.adult = adult;
            this.backdrop_path = backdrop_path;
            this.original_language = original_language;
            this.original_title = original_title;
            this.genre_ids = genre_ids;
            this.title = title;
            this.vote_average = vote_average;
            this.overview = overview;
            this.release_date = release_date;
        }

        public String getPopularity() {
            return popularity;
        }

        public int getVote_count() {
            return vote_count;
        }

        public boolean isVideo() {
            return video;
        }

        public String getPoster_path() {
            return poster_path;
        }

        public int getId() {
            return id;
        }

        public boolean isAdult() {
            return adult;
        }

        public String getBackdrop_path() {
            return backdrop_path;
        }

        public String getOriginal_language() {
            return original_language;
        }

        public String getOriginal_title() {
            return original_title;
        }

        public ArrayList<Integer> getGenre_ids() {
            return genre_ids;
        }

        public String getTitle() {
            return title;
        }

        public int getVote_average() {
            return vote_average;
        }

        public String getOverview() {
            return overview;
        }

        public String getRelease_date() {
            return release_date;
        }
}


/*
    {
      "popularity": 272.71,
      "vote_count": 1901,
      "video": false,
      "poster_path": "/bB42KDdfWkOvmzmYkmK58ZlCa9P.jpg",
      "id": 512200,
      "adult": false,
      "backdrop_path": "/hreiLoPysWG79TsyQgMzFKaOTF5.jpg",
      "original_language": "en",
      "original_title": "Jumanji: The Next Level",
      "genre_ids": [
        28,
        12,
        35,
        14
      ],
      "title": "Jumanji: The Next Level",
      "vote_average": 6.8,
      "overview": "As the gang return to Jumanji to rescue one of their own, they discover that nothing is as they expect. The players will have to brave parts unknown and unexplored in order to escape the world’s most dangerous game.",
      "release_date": "2019-12-04"
    }
 */