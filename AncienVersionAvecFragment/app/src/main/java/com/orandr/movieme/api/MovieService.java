package com.orandr.movieme.api;

import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.ListNoDateCollection;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface MovieService {




    /*

    {
  "page": 1,
  "total_results": 4,
  "total_pages": 1,
  "results": [
    {
      "popularity": 24.861,
      "vote_count": 8603,
      "video": false,
      "poster_path": "/22hqf97LadMvkd4zDi3Bq25xSqD.jpg",
      "id": 353486,
      "adult": false,
      "backdrop_path": "/rz3TAyd5kmiJmozp3GUbYeB5Kep.jpg",
      "original_language": "en",
      "original_title": "Jumanji: Welcome to the Jungle",
      "genre_ids": [
        28,
        12,
        35,
        14
      ],
      "title": "Jumanji: Welcome to the Jungle",
      "vote_average": 6.7,
      "overview": "The tables are turned as four teenagers are sucked into Jumanji's world - pitted against rhinos, black mambas and an endless variety of jungle traps and puzzles. To survive, they'll play as characters from the game.",
      "release_date": "2017-12-09"
    },
    {
      "popularity": 15.788,
      "id": 6795,
      "video": false,
      "vote_count": 1893,
      "vote_average": 6.3,
      "title": "Zathura: A Space Adventure",
      "release_date": "2005-11-06",
      "original_language": "en",
      "original_title": "Zathura: A Space Adventure",
      "genre_ids": [
        10751,
        14,
        878,
        12,
        35
      ],
      "backdrop_path": "/jzcOVeydNYF4hX0OiNftWC8QZJz.jpg",
      "adult": false,
      "overview": "After their father is called into work, two young boys, Walter and Danny, are left in the care of their teenage sister, Lisa, and told they must stay inside. Walter and Danny, who anticipate a boring day, are shocked when they begin playing Zathura, a space-themed board game, which they realize has mystical powers when their house is shot into space. With the help of an astronaut, the boys attempt to return home.",
      "poster_path": "/amqgIuISRBt8tsZM6cTT6gO9WLR.jpg"
    }
  ]
}





"results": [
    {
      "popularity": 272.71,
      "vote_count": 1901,
      "video": false,
      "poster_path": "/bB42KDdfWkOvmzmYkmK58ZlCa9P.jpg",
      "id": 512200,
      "adult": false,
      "backdrop_path": "/hreiLoPysWG79TsyQgMzFKaOTF5.jpg",
      "original_language": "en",
      "original_title": "Jumanji: The Next Level",
      "genre_ids": [
        28,
        12,
        35,
        14
      ],
      "title": "Jumanji: The Next Level",
      "vote_average": 6.8,
      "overview": "As the gang return to Jumanji to rescue one of their own, they discover that nothing is as they expect. The players will have to brave parts unknown and unexplored in order to escape the world’s most dangerous game.",
      "release_date": "2019-12-04"
    }

    ],
  "page": 1,
  "total_results": 1138,
  "dates": {
    "maximum": "2020-03-02",
    "minimum": "2020-01-14"
  },
  "total_pages": 57
}


    */

    /*
    {"type":"FeatureCollection",
            "features":[{
                        "type":"Feature",
                        "properties":
                            {"NOM":"Agence de Mobilité StationMobile",
                             "RUE":"15, boulevard Joseph Vallier",
                             "CODEPOSTAL":38000,
                             "COMMUNE":"GRENOBLE",
                             "TYPE":"Agence Mobilité",
                             "type":"agenceM",
                             "CODE":1,
                             "LaMetro":true,
                             "LeGresivaudan":false,
                            "PaysVoironnais":false,
                            "id":1
                            },
                            "geometry":
                                {
                                    "type":"Point",
                                    "coordinates":[5.71308,45.17995]
                                }
                        }
            ]
    }

    @GET("/api/findType/json")
    Call<FeatureCollection> getMovieList(@Query("types") String type);


    @GET("/api/findType/json")
    Call<FeatureCollection> getMovie(@Query("types") String type, @Query("query") String name);

    //https://data.metromobilite.fr/api/findType/json?types=agenceM&query=Agence+de+Mobilit%C3%A9+StationMobile

    */


    //   https://api.themoviedb.org/3/movie/now_playing?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1
    //   https://api.themoviedb.org/3/movie/popular?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1
    //   https://api.themoviedb.org/3/movie/upcoming?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&page=1


    //   https://api.themoviedb.org/3/search/movie?api_key=1abe855bc465dce9287da07b08a664eb&language=en-US&query=jumanji&page=1


    @GET("/movies/upcoming")
    Call<ListCollection> getUpcoming(@Query("api_key") String api_key, @Query("language") String language);


    @GET("/movies/upcoming")
    Call<ListCollection> getUpcomingPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") Integer num_page);


    @GET("/movies/now_playing")
    Call<ListCollection> getNowPlaying(@Query("api_key") String api_key, @Query("language") String language);


    @GET("/movies/now_playing")
    Call<ListCollection> getNowPlayingPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") Integer num_page);


    @GET("/movies/popular")
    Call<ListNoDateCollection> getPopular(@Query("api_key") String api_key, @Query("language") String language);


    @GET("/movies/popular")
    Call<ListNoDateCollection> getPopularPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") Integer num_page);


    @GET("/search/movie")
    Call<ListNoDateCollection> getSearch(@Query("api_key") String api_key, @Query("language") String language, @Query("query") String query);


    @GET("/search/movie")
    Call<ListNoDateCollection> getSearchPage(@Query("api_key") String api_key, @Query("language") String language, @Query("query") String query, @Query("page") Integer num_page);




}
