package com.orandr.movieme;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.tabs.TabLayout;
import com.orandr.movieme.api.MovieService;
import com.orandr.movieme.api.RecyclerViewAdapter;
import com.orandr.movieme.api.RetrofitClient;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.Properties;
import com.orandr.movieme.page.PagerAdapter;
import com.orandr.movieme.page.Tab1;
import com.orandr.movieme.page.Tab2;
import com.orandr.movieme.page.Tab3;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Tab1.OnFragmentInteractionListener, Tab2.OnFragmentInteractionListener, Tab3.OnFragmentInteractionListener {


    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;

    public static final String ID_PROPERTIE = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TabLayout tabs = findViewById(R.id.tablayout);


        tabs.addTab(tabs.newTab().setText(R.string.tab_text_1));
        tabs.addTab(tabs.newTab().setText(R.string.tab_text_2));
        tabs.addTab(tabs.newTab().setText(R.string.tab_text_3));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);

        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerViewAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Properties resultList) {
                Intent intent = new Intent(getApplicationContext(), Tab2.class);
                intent.putExtra(ID_PROPERTIE, resultList.getId());
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(recyclerViewAdapter);
        fetchDataPopular();






        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabs.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




//        Bundle bundle = getIntent().getExtras();
//        if(bundle!=null) {
//
//        }


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }



    private final String BASE_URL = "https://developers.themoviedb.org/3/";
    private final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    private final String LANGUAGE = "en-US";

    private void fetchDataPopular(){
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getNowPlaying(API_KEY,LANGUAGE).enqueue(new Callback<ListCollection>() {
            @Override
            public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                if(response.isSuccessful() && response.body() != null) {
                    //Manage Data
                    ListCollection collection = response.body();
                    recyclerViewAdapter.addFeatureList(collection.getResult());
                }else{
                    Toast.makeText(getApplicationContext(),getString(R.string.app_error),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ListCollection> call, Throwable t) {

            }
        });
    }
}