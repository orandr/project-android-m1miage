package com.orandr.movieme.api;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.orandr.movieme.R;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.ListNoDateCollection;
import com.orandr.movieme.model.Properties;
import com.orandr.movieme.model.PropertiesCollection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Properties> propertiesList;
    private OnItemClickListener listener;

    public RecyclerViewAdapter(List<Properties> propertiesList, OnItemClickListener listener) {
        this.propertiesList = propertiesList;
        this.listener = listener;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, Properties resultList);
    }

    @Override
    public int getItemCount() {
        return this.propertiesList.size();
    }

    public void addFeatureList(List<Properties> resultList){
        this.propertiesList.addAll(resultList);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tab1,parent,false);
        return new RecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        final Properties properties = propertiesList.get(position);

//        holder.image.setImageResource(R.drawable.metro_logo);
        holder.propertieId.setText(properties.getId());
        holder.propertiePosterPath.setText(properties.getPoster_path());
        holder.propertieTitle.setText(properties.getTitle());
        holder.propertieVoteAverage.setText(properties.getVote_average());
        holder.propertieReleaseDate.setText(properties.getRelease_date());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, properties);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View itemView;
        final TextView propertieId;
        final TextView propertiePosterPath;
        final TextView propertieTitle;
        final TextView propertieVoteAverage;
        final TextView propertieReleaseDate;
//        final ImageView  poster;

        ViewHolder(View view) {
            super(view);
            itemView = view;
            propertieId = view.findViewById(R.id.propertie_id);
            propertiePosterPath = view.findViewById(R.id.propertie_poster_path);
            propertieTitle = view.findViewById(R.id.propertie_title);
            propertieVoteAverage = view.findViewById(R.id.propertie_vote_average);
            propertieReleaseDate = view.findViewById(R.id.propertie_release_date);
        }

    }


}
