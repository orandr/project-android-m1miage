package com.orandr.movieme.page;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.orandr.movieme.R;
import com.orandr.movieme.api.FetchMovie;
import com.orandr.movieme.api.MovieService;
import com.orandr.movieme.api.RetrofitClient;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.Properties;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tab1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tab1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tab1 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FetchMovie f;
    private View v;
    private TextView propertie_id;
    private TextView propertie_poster_path;
    private TextView propertie_title;
    private TextView propertie_vote;
    private TextView propertie_release;


    private OnFragmentInteractionListener mListener;

    public Tab1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tab1.
     */
    // TODO: Rename and change types and number of parameters
    public static Tab1 newInstance(String param1, String param2) {
        Tab1 fragment = new Tab1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        fetchDataNowPlaying();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v =  inflater.inflate(R.layout.fragment_tab1, container, false);
        propertie_id = (TextView) v.findViewById(R.id.propertie_id);
        propertie_poster_path = (TextView) v.findViewById(R.id.propertie_poster_path);
        propertie_release = (TextView) v.findViewById(R.id.propertie_release_date);
        propertie_title = (TextView) v.findViewById(R.id.propertie_title);
        propertie_vote = (TextView) v.findViewById(R.id.propertie_vote_average);




//        Toast.makeText(getContext(),"Fin", Toast.LENGTH_SHORT).show();
        return v;

        // Inflate the layout for this fragment
    }


    private void displayData(Properties p) {
//        propertie_id.setText("idText");
//        propertie_poster_path.setText("posterText");
//        propertie_release.setText("releaseText");
//        propertie_title.setText("titleText");
//        propertie_vote.setText("voteText");

        Toast.makeText(getContext(),p.getId(),Toast.LENGTH_SHORT).show();
        propertie_id.setText(p.getId());
        propertie_poster_path.setText(p.getPoster_path());
        propertie_release.setText(p.getRelease_date());
        propertie_title.setText(p.getTitle());
        propertie_vote.setText(p.getVote_average());
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    private final String BASE_URL = "https://developers.themoviedb.org/3/";
    private final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    private final String LANGUAGE = "en-US";


    private void fetchDataNowPlaying(){
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getNowPlaying(API_KEY,LANGUAGE).enqueue(new Callback<ListCollection>() {
            @Override
            public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                if(response.isSuccessful() && response.body() != null) {
                    //Manage Data
                    ListCollection collection = response.body();
                    displayData(collection.getResult().get(0));
                }else{
                    Toast.makeText(getContext(),getString(R.string.app_error),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ListCollection> call, Throwable t) {

            }
        });
    }
}
