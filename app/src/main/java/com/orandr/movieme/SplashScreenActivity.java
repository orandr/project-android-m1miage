package com.orandr.movieme;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

import com.orandr.movieme.MainActivity;
import com.orandr.movieme.R;

public class SplashScreenActivity extends AppCompatActivity {

    private final int TEMPS_DE_CHARGEMENT = 1500;
    ProgressBar splashProgress;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //Redirige vers la page principale apres le chargement de l'application
        splashProgress = findViewById(R.id.splashProgress);
        playProgress();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //demarrer une page à travers un Intent
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //Lancer l'activité
                startActivity(intent);
                //Finir immédiatement après l'activité SplashScreen
                finish();

            }
        };

        new Handler().postDelayed(runnable, TEMPS_DE_CHARGEMENT);


    }

    private void playProgress() {
        ObjectAnimator.ofInt(splashProgress, "progress", 100)
                .setDuration(TEMPS_DE_CHARGEMENT)
                .start();
    }
}
