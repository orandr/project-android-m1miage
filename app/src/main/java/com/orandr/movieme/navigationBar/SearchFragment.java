package com.orandr.movieme.navigationBar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.orandr.movieme.PropertieDetailsActivity;
import com.orandr.movieme.R;
import com.orandr.movieme.api.FetchMovie;
import com.orandr.movieme.model.Properties;
import com.orandr.movieme.recycler.RecyclerViewAdapter;
import org.parceler.Parcels;
import java.util.ArrayList;

public class SearchFragment extends Fragment {

    Context context;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerAdapter;
    SearchView searchView;
    DividerItemDecoration dividerItemDecoration;
    ArrayList<Properties> arrayList;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView message;


    public static final String PROPERTIE_MOVIE = "propertie_movie";
    private static final int TEMPS_REFRESH = 1000;
    private static final String TAG = "SearchFragment";

    private FetchMovie f;

    public SearchFragment(Context context) {
        this.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        f = new FetchMovie(context);

        message = view.findViewById(R.id.textVide);

        recyclerView = view.findViewById(R.id.recyclerView_search);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);

        recyclerView.addItemDecoration(dividerItemDecoration);

        arrayList = new ArrayList<>();

        recyclerAdapter = new RecyclerViewAdapter(arrayList, new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Properties properties) {
                Intent intent = new Intent(context, PropertieDetailsActivity.class);
                Parcelable parcelable = Parcels.wrap(properties);
                intent.putExtra(PROPERTIE_MOVIE, parcelable);
                startActivity(intent);
            }
        }, context);


        recyclerView.setAdapter(recyclerAdapter);

        searchView = view.findViewById(R.id.search_bar);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                f.fetchMovieData_getSearch(query, recyclerAdapter);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                arrayList.clear();
                f.fetchMovieData_getSearch(newText, recyclerAdapter);
                return false;
            }
        });

        if(!searchView.hasFocus()) {
            message.setText("Aucun resultat");
        }

        return view;
    }

}
