package com.orandr.movieme.navigationBar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.orandr.movieme.PropertieDetailsActivity;
import com.orandr.movieme.R;
import com.orandr.movieme.model.Properties;
import com.orandr.movieme.recycler.RecyclerViewAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;

import static com.orandr.movieme.toolbar.Frag4_top_rated.PROPERTIE_MOVIE;


public class FavoritesFragment extends Fragment {

    private static final int TEMPS_REFRESH = 1000;
    private static final String TAG = "FavoritesFragment";
    private Context context;
    private RecyclerView recyclerView;
    private ImageView addFavorites;
    private static TextView message;
    private static RecyclerViewAdapter recyclerAdapter;
    private static ArrayList<Properties> listPropertiesFavorites = new ArrayList<>();

    public FavoritesFragment(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_favorites, container, false);
        addFavorites = view.findViewById(R.id.add_favorite_image);
        message = view.findViewById(R.id.textVide);

        if(listPropertiesFavorites.size() != 0) {

                recyclerView = view.findViewById(R.id.recyclerViewFavoriteFragment);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));

                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);

                recyclerView.addItemDecoration(dividerItemDecoration);

                recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, Properties properties) {
                        Intent intent = new Intent(context, PropertieDetailsActivity.class);
                        Parcelable parcelable = Parcels.wrap(properties);
                        intent.putExtra(PROPERTIE_MOVIE, parcelable);
                        startActivity(intent);
                    }
                }, context);

                this.recyclerAdapter.addFeatureList(listPropertiesFavorites);


                recyclerView.setAdapter(recyclerAdapter);
                message.setText("");

        } else{
            message.setText("Aucun Film en Favori");
        }


        content_refresh();


        return view;
    }

    public ArrayList<Properties> getListPropertiesFavorites() {
        return listPropertiesFavorites;
    }

    public void setListPropertiesFavorites(ArrayList<Properties> listPropertiesFavorites) {
        this.listPropertiesFavorites = listPropertiesFavorites;
    }

    public void addList(Properties properties) {
        if(this.listPropertiesFavorites.contains(properties)){
            Toast.makeText(context, "Déjà dans les favoris", Toast.LENGTH_LONG).show();
        }else{
            this.listPropertiesFavorites.add(properties);
            Toast.makeText(context, "Film "+properties.getTitle()+" ajouté au favori", Toast.LENGTH_LONG).show();
        }
    }


    public void removeList(RecyclerViewAdapter recyclerAdapter, Properties properties) {
        for(int i=0; i< this.listPropertiesFavorites.size(); i++) {
            if(this.listPropertiesFavorites.get(i).getId() == properties.getId() ) {
                this.listPropertiesFavorites.remove(i);
                this.context.unregisterComponentCallbacks(this);
                Toast.makeText(context, "Film "+properties.getTitle()+" supprimé au favori", Toast.LENGTH_LONG).show();
                if(recyclerAdapter != null ) {
                    recyclerAdapter.addFeatureList(listPropertiesFavorites);
                }else {
//                    Toast.makeText(context, "vide ", Toast.LENGTH_LONG).show();
                      Log.e(TAG, "vide ");
                }
            }
        }
    }

    public Properties getElementList(int position) {
        return this.listPropertiesFavorites.get(position);
    }

    public int getSizeList() {
        return this.listPropertiesFavorites.size();
    }

    public boolean noContain(Properties properties) {
        for(int i=0; i< this.listPropertiesFavorites.size(); i++) {
            if(this.listPropertiesFavorites.get(i).getId() == properties.getId() ) {
                return false;
            }
        }
        return true;
    }


    public RecyclerViewAdapter getRecyclerAdapter() {
        return this.recyclerAdapter;
    }

    public void setRecyclerAdapter(RecyclerViewAdapter recyclerAdapter) {
        this.recyclerAdapter = recyclerAdapter;
    }



    public void content_refresh() {
        if(this.listPropertiesFavorites.size() == 0) {
            message.setText("Aucun Film en Favori");
        } else {
            message.setText("");
        }
        refresh();
    }


    public void refresh() {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                content_refresh();
            }
        };

        handler.postDelayed(runnable, TEMPS_REFRESH);
    }



}
