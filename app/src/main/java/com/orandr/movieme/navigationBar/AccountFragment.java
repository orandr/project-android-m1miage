package com.orandr.movieme.navigationBar;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.orandr.movieme.R;

public class AccountFragment extends Fragment {
    private Context context;

    public AccountFragment(Context context) {
        this.context = context;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        TextView textView = view.findViewById(R.id.textView);
        FavoritesFragment f = new FavoritesFragment(context);
        textView.setText("Il y a "+f.getSizeList()+" film en favori");

        Button deconnexion = view.findViewById(R.id.button_deconnexion);
        Button connexion = view.findViewById(R.id.button_connexion);

        deconnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Déconnexion...", Toast.LENGTH_LONG).show();
            }
        });


        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Connexion...", Toast.LENGTH_LONG).show();
            }
        });
        return view;

    }


}
