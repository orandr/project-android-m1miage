package com.orandr.movieme.model;

import java.util.ArrayList;

public class Credits{
    public Integer id;
    public ArrayList<Cast> cast;
    public ArrayList<Crew> crews;

    public Credits() {}


    public Credits(Integer id, ArrayList<Cast> cast, ArrayList<Crew> crews) {
        this.id = id;
        this.cast = cast;
        this.crews = crews;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<Cast> getCast() {
        return cast;
    }

    public void setCast(ArrayList<Cast> cast) {
        this.cast = cast;
    }

    public ArrayList<Crew> getCrews() {
        return crews;
    }

    public void setCrews(ArrayList<Crew> crews) {
        this.crews = crews;
    }
}
