package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;

public class Crew {
    @SerializedName("credit_id")
    public String credit_id;
    @SerializedName("departement")
    public String departement;
    @SerializedName("gender")
    public Integer gender;
    @SerializedName("id")
    public Integer id;
    @SerializedName("job")
    public String job;
    @SerializedName("name")
    public String name;
    @SerializedName("profile_path")
    public String profile_path;

    public Crew(String credit_id, String departement, Integer gender, Integer id, String job, String name, String profile_path) {
        this.credit_id = credit_id;
        this.departement = departement;
        this.gender = gender;
        this.id = id;
        this.job = job;
        this.name = name;
        this.profile_path = profile_path;
    }

    public String getCredit_id() {
        return credit_id;
    }

    public void setCredit_id(String credit_id) {
        this.credit_id = credit_id;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }
}
