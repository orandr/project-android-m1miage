package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;

public class Production_countries {
    @SerializedName("iso_3166_1")
    private String iso_3166_1;
    @SerializedName("name")
    private String name;
}
