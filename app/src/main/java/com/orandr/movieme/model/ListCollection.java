package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListCollection {
    @SerializedName("results")
    private ArrayList<Properties> result;
    @SerializedName("page")
    private int page;
    @SerializedName("total_results")
    private int total_results;
    @SerializedName("dates")
    private Dates dates;
    @SerializedName("total_pages")
    private int total_pages;

    public ListCollection(ArrayList<Properties> result, int page, int total_results, Dates dates, int total_pages) {
        this.result = result;
        this.page = page;
        this.total_results = total_results;
        this.dates = dates;
        this.total_pages = total_pages;
    }


    public ArrayList<Properties> getResult() {
        return result;
    }

    public int getPage() {
        return page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public Dates getDates() {
        return dates;
    }

    public int getTotal_pages() {
        return total_pages;
    }
}
