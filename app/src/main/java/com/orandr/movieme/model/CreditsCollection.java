package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class CreditsCollection{
    @SerializedName("id")
    public Integer id;
    @SerializedName("cast")
    public ArrayList<Cast> cast;
    @SerializedName("crew")
    public ArrayList<Crew> crews;

    public CreditsCollection() {
        this.id = 0;
        this.cast = new ArrayList<>();
        this.crews = new ArrayList<>();
    }
    public CreditsCollection(int id, ArrayList<Cast> cast, ArrayList<Crew> crews) {
        this.id = id;
        this.cast = cast;
        this.crews = crews;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Cast> getCast() {
        return cast;
    }

    public ArrayList<Crew> getCrews() {
        return crews;
    }


    public void setCast(ArrayList<Cast> cast) {
        this.cast = cast;
    }

    public void setCrews(ArrayList<Crew> crews) {
        this.crews = crews;
    }

    public String getNomRealisateur() {
        for(int i=0; i< crews.size(); i++) {
            if(crews.get(i).getJob().equals("Director")) {
                return crews.get(i).getName();
            }
        }
        return null;
    }


    public String getNomActeur() {
        for(int i=0; i< crews.size(); i++) {
            if(crews.get(i).getJob().equals("Producer")) {
                return crews.get(i).getName();
            }
        }
        return null;
    }

}
