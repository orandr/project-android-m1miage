package com.orandr.movieme.model;

public class MovieSimilar {
    private String title;
    private String release_date;
    private String backdrop_path;
    private String overview;
    private String poster_path;
    private Float vote_average;
    private Float popularity;
    private Integer id;

    public MovieSimilar() {
        this.title = "";
        this.release_date = "";
        this.backdrop_path = "";
        this.overview = "";
        this.poster_path = "";
        this.vote_average = new Float(0);
        this.popularity = new Float(0);
        this.id = 0;
    }

    public MovieSimilar(String title, String release_date, String backdrop_path, String overview, String poster_path, Float vote_average, Float popularity, Integer id) {
        this.title = title;
        this.release_date = release_date;
        this.backdrop_path = backdrop_path;
        this.overview = overview;
        this.poster_path = poster_path;
        this.vote_average = vote_average;
        this.popularity = popularity;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public Float getVote_average() {
        return vote_average;
    }

    public void setVote_average(Float vote_average) {
        this.vote_average = vote_average;
    }

    public Float getPopularity() {
        return popularity;
    }

    public void setPopularity(Float popularity) {
        this.popularity = popularity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
