package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class VideosCollection {
    @SerializedName("results")
    private ArrayList<Video> results;
    @SerializedName("id")
    private Integer id;


    public VideosCollection(ArrayList<Video> results, Integer id) {
        this.results = results;
        this.id = id;
    }

    public ArrayList<Video> getResults() {
        return results;
    }

    public void setResults(ArrayList<Video> results) {
        this.results = results;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
