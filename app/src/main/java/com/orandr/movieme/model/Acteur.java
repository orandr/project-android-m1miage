package com.orandr.movieme.model;

public class Acteur {
    private String nom;
    private String metier;
    private String imageProfile;
    private Integer sexe;


    public Acteur() {
        this.nom = "";
        this.metier = "";
        this.imageProfile = "";
        this.sexe = 1;
    }

    public Acteur(String nom, String metier, String imageProfile, Integer sexe) {
        this.nom = nom;
        this.metier = metier;
        this.imageProfile = imageProfile;
        this.sexe = sexe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMetier() {
        return metier;
    }

    public void setMetier(String metier) {
        this.metier = metier;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }

    public Integer getSexe() {
        return sexe;
    }

    public void setSexe(Integer sexe) {
        this.sexe = sexe;
    }
}
