package com.orandr.movieme.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DetailsCollection {





    @SerializedName("adult")
    private Boolean adult;
    @SerializedName("backdrop_path")
    private String backdrop_path;
    @SerializedName("belongs_to_collection")
    private Belongs_to_collection belongs_to_collection;
    @SerializedName("budget")
    private Integer budget;
    @SerializedName("genres")
    private ArrayList<Genres> genres;
    @SerializedName("homepage")
    private String homepage;
    @SerializedName("id")
    private Integer id;
    @SerializedName("imdb_id")
    private String imdb_id;
    @SerializedName("original_language")
    private String original_language;
    @SerializedName("original_title")
    private String original_title;
    @SerializedName("overview")
    private String overview;
    @SerializedName("popularity")
    private Float popularity;
    @SerializedName("poster_path")
    private String poster_path;
    @SerializedName("production_companies")
    private ArrayList<Production_companies> production_companies;
    @SerializedName("production_countries")
    private ArrayList<Production_countries> production_countries;
    @SerializedName("release_date")
    private String release_date;
    @SerializedName("revenue")
    private Integer revenue;
    @SerializedName("runtime")
    private Integer runtime;
    @SerializedName("spoken_languages")
    private ArrayList<Spoken_languages> spoken_languages;
    @SerializedName("status")
    private String status;
    @SerializedName("tagline")
    private String tagline;
    @SerializedName("title")
    private String title;
    @SerializedName("video")
    private Boolean video;
    @SerializedName("vote_average")
    private Float vote_average;
    @SerializedName("vote_count")
    private Integer vote_count;

    public DetailsCollection() {
        this.adult = false;
        this.backdrop_path = "";
        this.genres = new ArrayList<>();
        this.homepage = "";
        this.id = 0;
        this.original_language = "";
        this.original_title = "";
        this.overview = "";
        this.popularity = new Float(0);
        this.poster_path = "";
        this.runtime = 0;
        this.title = "";
        this.video = false;
        this.vote_average = new Float(0);
        this.vote_count = 0;
        this.release_date = "";
        this.belongs_to_collection = new Belongs_to_collection();
        this.budget = budget;
        this.imdb_id = imdb_id;
        this.revenue = revenue;
        this.status = status;
        this.tagline = tagline;
        this.spoken_languages = new ArrayList<>();
        this.production_companies = new ArrayList<>();
        this.production_countries = new ArrayList<>();
    }

    public DetailsCollection(Boolean adult, String backdrop_path, Belongs_to_collection belongs_to_collection, Integer budget, ArrayList<Genres> genres, String homepage, Integer id, String imdb_id, String original_language, String original_title, String overview, Float popularity, String poster_path, ArrayList<Production_companies> production_companies, ArrayList<Production_countries> production_countries, String release_date, Integer revenue, Integer runtime, ArrayList<Spoken_languages> spoken_languages, String status, String tagline, String title, Boolean video, Float vote_average, Integer vote_count) {
        this.adult = adult;
        this.backdrop_path = backdrop_path;
        this.belongs_to_collection = belongs_to_collection;
        this.budget = budget;
        this.genres = genres;
        this.homepage = homepage;
        this.id = id;
        this.imdb_id = imdb_id;
        this.original_language = original_language;
        this.original_title = original_title;
        this.overview = overview;
        this.popularity = popularity;
        this.poster_path = poster_path;
        this.production_companies = production_companies;
        this.production_countries = production_countries;
        this.release_date = release_date;
        this.revenue = revenue;
        this.runtime = runtime;
        this.spoken_languages = spoken_languages;
        this.status = status;
        this.tagline = tagline;
        this.title = title;
        this.video = video;
        this.vote_average = vote_average;
        this.vote_count = vote_count;
    }

    public Boolean getAdult() {
        return adult;
    }

    public void setAdult(Boolean adult) {
        this.adult = adult;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public Belongs_to_collection getBelongs_to_collection() {
        return belongs_to_collection;
    }

    public void setBelongs_to_collection(Belongs_to_collection belongs_to_collection) {
        this.belongs_to_collection = belongs_to_collection;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public ArrayList<Genres> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genres> genres) {
        this.genres = genres;
    }


    public String getGenresCollection() {
        String genresConcat = "";
        if(2 <= this.genres.size()) {
            genresConcat = this.genres.get(1).getName()+", "+this.genres.get(0).getName();
        } else if(1 <= this.genres.size()) {
            genresConcat = this.genres.get(0).getName();
        }
        return genresConcat;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public void setImdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public Float getPopularity() {
        return popularity;
    }

    public void setPopularity(Float popularity) {
        this.popularity = popularity;
    }

    public String getPoster_path() {
        return "https://image.tmdb.org/t/p/original"+poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public ArrayList<Production_companies> getProduction_companies() {
        return production_companies;
    }

    public void setProduction_companies(ArrayList<Production_companies> production_companies) {
        this.production_companies = production_companies;
    }

    public ArrayList<Production_countries> getProduction_countries() {
        return production_countries;
    }

    public void setProduction_countries(ArrayList<Production_countries> production_countries) {
        this.production_countries = production_countries;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public Integer getRevenue() {
        return revenue;
    }

    public void setRevenue(Integer revenue) {
        this.revenue = revenue;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    public ArrayList<Spoken_languages> getSpoken_languages() {
        return spoken_languages;
    }

    public void setSpoken_languages(ArrayList<Spoken_languages> spoken_languages) {
        this.spoken_languages = spoken_languages;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getVideo() {
        return video;
    }

    public void setVideo(Boolean video) {
        this.video = video;
    }

    public Float getVote_average() {
        return vote_average;
    }

    public void setVote_average(Float vote_average) {
        this.vote_average = vote_average;
    }

    public Integer getVote_count() {
        return vote_count;
    }

    public void setVote_count(Integer vote_count) {
        this.vote_count = vote_count;
    }

}
