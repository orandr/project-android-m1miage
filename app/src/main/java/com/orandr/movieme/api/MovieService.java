package com.orandr.movieme.api;

import com.google.gson.JsonObject;
import com.orandr.movieme.model.CreditsCollection;
import com.orandr.movieme.model.DetailsCollection;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.VideosCollection;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface MovieService {

    @GET("/3/search/movie")
    Call<ListCollection> getSearch(@Query("query") String query, @Query("api_key") String api_key, @Query("language") String language);

    @GET("/3/movie/now_playing")
    Call<ListCollection> getNowPlaying(@Query("api_key") String api_key, @Query("language") String language);

    @GET("/3/movie/now_playing")
    Call<ListCollection> getNowPlayingPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int num_page);

    @GET("/3/movie/top_rated")
    Call<ListCollection> getTopRated(@Query("api_key") String api_key, @Query("language") String language);

    @GET("/3/movie/top_rated")
    Call<ListCollection> getTopRatedPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int num_page);

    @GET("/3/movie/upcoming")
    Call<ListCollection> getUpcoming(@Query("api_key") String api_key, @Query("language") String language);

    @GET("/3/movie/upcoming")
    Call<ListCollection> getUpcomingPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int num_page);

    @GET("/3/movie/popular")
    Call<ListCollection> getPopular(@Query("api_key") String api_key, @Query("language") String language);

    @GET("/3/movie/popular")
    Call<ListCollection> getPopularPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int num_page);

    @GET("/3/movie/{movie_id}/credits")
    Call<CreditsCollection> getMovieCredits( @Path("movie_id") int id, @Query("api_key") String api_key);


    @GET("/3/movie/{movie_id}/similar")
    Call<ListCollection> getSimilarMovie( @Path("movie_id") int id, @Query("api_key") String api_key, @Query("language") String language);


    @GET("/3/movie/{movie_id}/similar")
    Call<ListCollection> getSimilarMoviePage( @Path("movie_id") int id, @Query("page") int num_page, @Query("api_key") String api_key, @Query("language") String language);

    @GET("/3/movie/{movie_id}/videos")
    Call<VideosCollection> getVideo(@Path("movie_id") int id, @Query("api_key") String api_key, @Query("language") String language);


    @GET("/3/movie/{movie_id}")
    Call<DetailsCollection> getDetails(@Path("movie_id") int id, @Query("api_key") String api_key, @Query("language") String language);


}
