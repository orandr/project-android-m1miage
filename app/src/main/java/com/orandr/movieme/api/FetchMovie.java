package com.orandr.movieme.api;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.orandr.movieme.R;
import com.orandr.movieme.model.CreditsCollection;
import com.orandr.movieme.model.DetailsCollection;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.MovieSimilar;
import com.orandr.movieme.model.VideosCollection;
import com.orandr.movieme.recycler.RecyclerViewAdapter;
import com.orandr.movieme.recycler.RecyclerViewAdapter_details;
import com.orandr.movieme.recycler.RecyclerViewAdapter_details_acteur;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FetchMovie {
    private static final String TAG = "FetchMovie";
    private final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    private final String LANGUAGE = "fr-FR";
    private final Context context;
    private ListCollection collection;
//            "id": 508439,
//            "title": "Onward",

    public FetchMovie(Context context) {
        this.context = context;
    }

    public void fetchMovieData_getSearch(String query, final RecyclerViewAdapter recyclerAdapter) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getSearch(query, API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            collection = response.body();
                            recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_getSearch", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }

    public void fetchMovieData_TopRated(final RecyclerViewAdapter recyclerAdapter) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getTopRated(API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            collection = response.body();
                            recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_TopRated", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }


    public void fetchMovieData_TopRated_page(final RecyclerViewAdapter recyclerAdapter, int num_page) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getTopRatedPage(API_KEY, LANGUAGE, num_page)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            collection = response.body();
                            recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_TopRated", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }

    public void fetchMovieData_NowPlaying(final RecyclerViewAdapter recyclerAdapter) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getNowPlaying(API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            collection = response.body();
                            recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_NowPlaying", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }


    public void fetchMovieData_NowPlaying_page(final RecyclerViewAdapter recyclerAdapter, int num_page) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getNowPlayingPage(API_KEY, LANGUAGE, num_page)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            collection = response.body();
                            recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_NowPlaying", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }



    public void fetchMovieData_Upcoming(final RecyclerViewAdapter recyclerAdapter) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getUpcoming(API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            collection = response.body();
                            recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_Upcoming", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }


    public void fetchMovieData_Upcoming_page(final RecyclerViewAdapter recyclerAdapter, int num_page) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getUpcomingPage(API_KEY, LANGUAGE, num_page)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            collection = response.body();
                            recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_Upcoming", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }

    public void fetchMovieData_Popular(final RecyclerViewAdapter recyclerAdapter) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getPopular(API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            collection = response.body();
                            recyclerAdapter.addFeatureList(collection.getResult());
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_Popular", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }


    public void fetchMovieData_Video(final WebView webView, int movie_id) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getVideo(movie_id, API_KEY, LANGUAGE)
                .enqueue(new Callback<VideosCollection>() {
                    @Override
                    public void onResponse(Call<VideosCollection> call, Response<VideosCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            VideosCollection videosCollection = response.body();
                            String path = "";
                            for(int i = 0; i< videosCollection.getResults().size(); i++) {
                                if(!videosCollection.getResults().get(i).getKey().equals("") ) {
                                    path = videosCollection.getResults().get(i).getKey();
                                    i = videosCollection.getResults().size();
                                }
                            }

                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.setWebChromeClient(new WebChromeClient());
                            webView.loadData(path, "text/html", "utf-8");
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_Video", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<VideosCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }



    public void fetchMovieData_Detail(final TextView title, final ImageView poster_path, final TextView release_date, final TextView runtime, final TextView genres, final TextView adult, final TextView popularity, final TextView overview, final TextView vote_average, int movie_id) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getDetails(movie_id, API_KEY, LANGUAGE)
                .enqueue(new Callback<DetailsCollection>() {
                    @Override
                    public void onResponse(Call<DetailsCollection> call, Response<DetailsCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            DetailsCollection detailsCollection = response.body();
//                            Log.e(TAG, ""+detailsCollection);
//
                            title.setText(detailsCollection.getTitle());
                            Picasso.with(context)
                                .load(detailsCollection.getPoster_path())
                                .placeholder(R.mipmap.lolo1)
                                .error(R.drawable.aucune_image_disponible)
                                .into(poster_path);
                            release_date.setText(detailsCollection.getRelease_date());
                            genres.setText(detailsCollection.getGenresCollection()+"");
                            adult.setText(detailsCollection.getAdult()+"");
                            popularity.setText(detailsCollection.getPopularity()+"");
                            overview.setText(detailsCollection.getOverview());
                            vote_average.setText(detailsCollection.getVote_average()+"");

                            int temps = detailsCollection.getRuntime();
                            if(temps / 60 < 1) {
                                runtime.setText(temps+" min");
                            } else {
                                int heure = temps / 60;
                                int min = temps % 60;
                                runtime.setText(heure +" h "+min+" min");
                            }


                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_Video", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DetailsCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }
//
//    public void fetchMovieData_Movie(final TextView realisateur, final ImageView acteur_1, final ImageView acteur_2, final ImageView acteur_3, final ImageView acteur_4, int movie_id) {
//        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
//        movieService.getMovieCredits(movie_id, API_KEY)
//                .enqueue(new Callback<CreditsCollection>() {
//                    @Override
//                    public void onResponse(Call<CreditsCollection> call, Response<CreditsCollection> response) {
//                        if( response.isSuccessful() && response.body() != null ) {
//                            //Manage Data
//                            CreditsCollection creditsCollection = response.body();
//                            realisateur.setText(""+creditsCollection.getNomRealisateur());
//                            for(int i =0 ; i< creditsCollection.getCast().size() && i< 4; i++) {
//                                if(i==0) {
//                                    acteur_1.setVisibility(View.VISIBLE);
//                                    Picasso.with(context)
//                                            .load(creditsCollection.getCast().get(i).getProfile_path())
//                                            .placeholder(R.mipmap.lolo1)
//                                            .error(R.drawable.aucune_image_disponible)
//                                            .into(acteur_1);
//                                } else if(i==1) {
//                                    acteur_2.setVisibility(View.VISIBLE);
//                                    Picasso.with(context)
//                                            .load(creditsCollection.getCast().get(i).getProfile_path())
//                                            .placeholder(R.mipmap.lolo1)
//                                            .error(R.drawable.aucune_image_disponible)
//                                            .into(acteur_2);
//                                } else if(i==2) {
//                                    acteur_3.setVisibility(View.VISIBLE);
//                                    Picasso.with(context)
//                                            .load(creditsCollection.getCast().get(i).getProfile_path())
//                                            .placeholder(R.mipmap.lolo1)
//                                            .error(R.drawable.aucune_image_disponible)
//                                            .into(acteur_3);
//                                } else if(i==3) {
//                                    acteur_4.setVisibility(View.VISIBLE);
//                                    Picasso.with(context)
//                                            .load(creditsCollection.getCast().get(i).getProfile_path())
//                                            .placeholder(R.mipmap.lolo1)
//                                            .error(R.drawable.aucune_image_disponible)
//                                            .into(acteur_4);
//                                }
//                            }
//
//                        } else {
//                            Toast.makeText(context, "Erreur, fetchMovieData_Movie", Toast.LENGTH_LONG).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<CreditsCollection> call, Throwable t) {
//                        //Manage errors
//                    }
//                });
//    }



    public void fetchMovieData_Movie(final TextView realisateur,  int movie_id, final RecyclerViewAdapter_details_acteur recyclerViewAdapter_details_acteur) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getMovieCredits(movie_id, API_KEY)
                .enqueue(new Callback<CreditsCollection>() {
                    @Override
                    public void onResponse(Call<CreditsCollection> call, Response<CreditsCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            CreditsCollection creditsCollection = response.body();
                            realisateur.setText(""+creditsCollection.getNomRealisateur());
                            recyclerViewAdapter_details_acteur.addFeatureList(creditsCollection.getCast());

                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_Movie", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CreditsCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }

    public void fetchMovieData_Similar_recycler( int movie_id, final RecyclerViewAdapter_details recyclerViewAdapter_details) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getSimilarMovie(movie_id, API_KEY, LANGUAGE)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            ListCollection similaireMovieList = response.body();
                            recyclerViewAdapter_details.addFeatureList(similaireMovieList.getResult());

                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_Popular", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }

}
