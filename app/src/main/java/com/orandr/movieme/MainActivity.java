package com.orandr.movieme;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.orandr.movieme.navigationBar.AccountFragment;
import com.orandr.movieme.navigationBar.FavoritesFragment;
import com.orandr.movieme.navigationBar.SearchFragment;
import com.orandr.movieme.toolbar.Frag1_popular;
import com.orandr.movieme.toolbar.Frag2_upcoming;
import com.orandr.movieme.toolbar.Frag3_now;
import com.orandr.movieme.toolbar.Frag4_top_rated;

public class MainActivity extends AppCompatActivity {
    private static final int FRAGMENT_DEPART = 3;
    FrameLayout frameLayout;
    LinearLayout layout_tab;
    FragmentManager fragmentManager;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkConnection();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        layout_tab = (LinearLayout) findViewById(R.id.layout_tab);
        frameLayout = (FrameLayout) findViewById(R.id.layout_frame);
        load_fragment_bottom(new Frag1_popular(getApplicationContext()));

        tab = (TabLayout) findViewById(R.id.tab);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        Tab_adapter tab_adapter = new Tab_adapter(getSupportFragmentManager(), tab.getTabCount(), getApplicationContext());
        viewPager.setCurrentItem(FRAGMENT_DEPART);

        viewPager.setAdapter(tab_adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));


        layout_tab.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);

        tab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }



    TabLayout tab;
    ViewPager viewPager;
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.nav_favorites:
                            layout_tab.setVisibility(View.GONE);
                            frameLayout.setVisibility(View.VISIBLE);
                            load_fragment_bottom(new FavoritesFragment(getApplicationContext()))  ;
                            return true;
                        case R.id.nav_account:
                            layout_tab.setVisibility(View.GONE);
                            frameLayout.setVisibility(View.VISIBLE);
                            load_fragment_bottom(new AccountFragment(getApplicationContext()));
                            return true;
                        case R.id.nav_search:
                            layout_tab.setVisibility(View.GONE);
                            frameLayout.setVisibility(View.VISIBLE);
                            load_fragment_bottom(new SearchFragment(getApplicationContext()));
                            return true;
                        case R.id.nav_dashboard:
                            layout_tab.setVisibility(View.VISIBLE);
                            frameLayout.setVisibility(View.GONE);
                            return true;
                    }
                    return false;
                }
            };

    Boolean load_fragment_bottom(Fragment fragment) {
        if(fragment != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.layout_frame,fragment).commit();
            return true;
        }
        return false;
    }


    public void checkConnection() {
        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if(activeNetwork != null) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI && activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET ) {
                Toast.makeText(this, "Wifi and Data Network Enabled ", Toast.LENGTH_SHORT).show();
            } else if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI ) {
                Toast.makeText(this, "Wifi Enabled", Toast.LENGTH_SHORT).show();
            } else if(activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET ) {
                Toast.makeText(this, "Data Network Enabled", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


}

class Tab_adapter extends FragmentStatePagerAdapter {

    int jumpLabTab;
    Context context;
    FragmentManager fm;
    public Tab_adapter(@NonNull FragmentManager fm, int jumpLabTab, Context context) {
        super(fm);
        this.jumpLabTab = jumpLabTab;
        this.context = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Frag1_popular popular = new Frag1_popular(context);
                return popular;
            case 1:
                Frag2_upcoming upcoming = new Frag2_upcoming(context);
                return upcoming;
            case 2:
                Frag3_now now = new Frag3_now(context);
                return now;
            case 3:
                Frag4_top_rated top_rated = new Frag4_top_rated(context);
                return top_rated;
        }
        return null;
    }

    @Override
    public int getCount() {
        return jumpLabTab;
    }
}