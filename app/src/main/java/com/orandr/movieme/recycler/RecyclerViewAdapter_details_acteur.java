package com.orandr.movieme.recycler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.orandr.movieme.R;
import com.orandr.movieme.model.Cast;
import com.orandr.movieme.model.DetailsCollection;
import com.orandr.movieme.model.Properties;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class RecyclerViewAdapter_details_acteur extends RecyclerView.Adapter<RecyclerViewAdapter_details_acteur.ViewHolder> {
    private ArrayList<Cast> castArrayList;
    private OnItemClickListener listener;
    private Context context;
    private String genreConcatener;

    private DetailsCollection detailsCollection;
    private static final String TAG = "RecyclerViewAdapter_details";

    public interface OnItemClickListener {
        void onItemClick(View view, Cast cast);
    }

    public RecyclerViewAdapter_details_acteur(ArrayList<Cast> propertiesList, Context context) {
        this.castArrayList = propertiesList;
        this.context = context;
        this.castArrayList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return this.castArrayList.size();
    }

    public void addFeatureList(ArrayList<Cast> castList) {
        this.castArrayList.clear();
        this.castArrayList.addAll(castList);
        notifyDataSetChanged();
    }

    public String getGenreConcatener() {
        return genreConcatener;
    }

    public void setGenreConcatener(String genreConcatener) {
        this.genreConcatener = genreConcatener;
    }

    public DetailsCollection getDetailsCollection() {
        return detailsCollection;
    }

    public void setDetailsCollection(DetailsCollection detailsCollection) {
        this.detailsCollection = detailsCollection;
    }

    public boolean estVide() {
        return (this.castArrayList.size() == 0);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_propertie_details, parent, false);
        return new RecyclerViewAdapter_details_acteur.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        Log.e(TAG, "onBindViewHolder");
        final Cast cast = castArrayList.get(position);

        Picasso.with(context)
                .load(cast.getProfile_path())
                .placeholder(R.mipmap.lolo1)
                .error(R.drawable.aucune_image_disponible)
                .into(holder.propertiePosterPath);

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View itemView;
        final ImageView propertiePosterPath;
        ViewHolder(View view) {
            super(view);
            itemView = view;
            propertiePosterPath = view.findViewById(R.id.poster_path_image);
        }

    }

}
