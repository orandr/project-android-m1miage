package com.orandr.movieme.recycler;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import com.orandr.movieme.R;
import com.orandr.movieme.model.DetailsCollection;
import com.orandr.movieme.model.Properties;
import com.orandr.movieme.navigationBar.FavoritesFragment;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private ArrayList<Properties> propertiesList;
    private OnItemClickListener listener;
    private Context context;
    private FavoritesFragment favoritesFragment;
    private String genreConcatener;

    private DetailsCollection detailsCollection;
    private static final String TAG = "RecyclerViewAdapter";

    public interface OnItemClickListener {
        void onItemClick(View view, Properties properties);
    }

    public RecyclerViewAdapter(ArrayList<Properties> propertiesList, OnItemClickListener listener, Context context) {
        this.propertiesList = propertiesList;
        this.listener = listener;
        this.context = context;
        this.propertiesList= new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return this.propertiesList.size();
    }

    public void addFeatureList(ArrayList<Properties> featureList) {
        this.propertiesList.clear();
        this.propertiesList.addAll(featureList);
        notifyDataSetChanged();
    }

    public String getGenreConcatener() {
        return genreConcatener;
    }

    public void setGenreConcatener(String genreConcatener) {
        this.genreConcatener = genreConcatener;
    }

    public DetailsCollection getDetailsCollection() {
        return detailsCollection;
    }

    public void setDetailsCollection(DetailsCollection detailsCollection) {
        this.detailsCollection = detailsCollection;
    }

    public boolean estVide() {
        return (this.propertiesList.size() == 0);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_propertie, parent, false);
//        Log.e(TAG, "onCreateViewHolder");
        return new RecyclerViewAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        Log.e(TAG, "onBindViewHolder");
        final Properties properties = propertiesList.get(position);
        holder.propertieTitle.setText(""+properties.getTitle());
        holder.propertieVoteAverage.setText(""+properties.getVote_average());
        holder.propertieReleaseDate.setText(""+properties.getRelease_date());

        Picasso.with(context)
                .load(properties.getBackdrop_path())
                .placeholder(R.mipmap.lolo1)
                .error(R.drawable.aucune_image_disponible)
                .into(holder.propertiePosterPath);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, properties);
            }
        });
        favoritesFragment = new FavoritesFragment(context);
        content_refresh(holder,properties);

        holder.addFavoriteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoritesFragment.addList(properties);
                content_refresh(holder,properties);
            }
        });


        holder.removeFavoriteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoritesFragment.removeList(favoritesFragment.getRecyclerAdapter(), properties);
                content_refresh(holder,properties);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View itemView;
        final ImageView propertiePosterPath;
        final TextView propertieTitle;
        final TextView propertieVoteAverage;
        final TextView propertieReleaseDate;
        final  Button addFavoriteImage;
        final Button removeFavoriteImage;
        ViewHolder(View view) {
            super(view);
            itemView = view;
            propertiePosterPath = view.findViewById(R.id.backdrop_path_image);
            propertieTitle = view.findViewById(R.id.propertie_title);
            propertieVoteAverage = view.findViewById(R.id.propertie_vote_average);
            propertieReleaseDate = view.findViewById(R.id.propertie_release_date);
            addFavoriteImage = view.findViewById(R.id.add_favorite_button);
            removeFavoriteImage = view.findViewById(R.id.remove_favorite_button);
        }

    }
    public void content_refresh(final ViewHolder holder ,Properties properties) {
        if(favoritesFragment.noContain(properties)) {
            holder.addFavoriteImage.setVisibility(View.VISIBLE);
            holder.removeFavoriteImage.setVisibility(View.INVISIBLE);
        } else {
            holder.addFavoriteImage.setVisibility(View.INVISIBLE);
            holder.removeFavoriteImage.setVisibility(View.VISIBLE);
        }
        refresh(holder,properties);
    }


    public void refresh(final ViewHolder holder, final Properties properties) {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                content_refresh(holder,properties);
            }
        };

        handler.postDelayed(runnable, 1000);
    }

}
