package com.orandr.movieme.recycler;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.orandr.movieme.R;
import com.orandr.movieme.model.DetailsCollection;
import com.orandr.movieme.model.Properties;
import com.orandr.movieme.navigationBar.FavoritesFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class RecyclerViewAdapter_details extends RecyclerView.Adapter<RecyclerViewAdapter_details.ViewHolder> {
    private ArrayList<Properties> propertiesList;
    private OnItemClickListener listener;
    private Context context;
    private String genreConcatener;

    private DetailsCollection detailsCollection;
    private static final String TAG = "RecyclerViewAdapter_details";

    public interface OnItemClickListener {
        void onItemClick(View view, Properties properties);
    }

    public RecyclerViewAdapter_details(ArrayList<Properties> propertiesList, OnItemClickListener listener, Context context) {
        this.propertiesList = propertiesList;
        this.listener = listener;
        this.context = context;
        this.propertiesList= new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return this.propertiesList.size();
    }

    public void addFeatureList(ArrayList<Properties> featureList) {
        this.propertiesList.clear();
        this.propertiesList.addAll(featureList);
        notifyDataSetChanged();
    }

    public String getGenreConcatener() {
        return genreConcatener;
    }

    public void setGenreConcatener(String genreConcatener) {
        this.genreConcatener = genreConcatener;
    }

    public DetailsCollection getDetailsCollection() {
        return detailsCollection;
    }

    public void setDetailsCollection(DetailsCollection detailsCollection) {
        this.detailsCollection = detailsCollection;
    }

    public boolean estVide() {
        return (this.propertiesList.size() == 0);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_propertie_details, parent, false);
//        Log.e(TAG, "onCreateViewHolder");
        return new RecyclerViewAdapter_details.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        Log.e(TAG, "onBindViewHolder");
        final Properties properties = propertiesList.get(position);

        Picasso.with(context)
                .load(properties.getPoster_path())
                .placeholder(R.mipmap.lolo1)
                .error(R.drawable.aucune_image_disponible)
                .into(holder.propertiePosterPath);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, properties);
            }
        });

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View itemView;
        final ImageView propertiePosterPath;
        ViewHolder(View view) {
            super(view);
            itemView = view;
            propertiePosterPath = view.findViewById(R.id.poster_path_image);
        }

    }

}
