package com.orandr.movieme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orandr.movieme.api.FetchMovie;
import com.orandr.movieme.model.Properties;
import com.orandr.movieme.navigationBar.FavoritesFragment;
import com.orandr.movieme.recycler.RecyclerViewAdapter_details;
import com.orandr.movieme.recycler.RecyclerViewAdapter_details_acteur;

import org.parceler.Parcels;
import java.util.ArrayList;

import static com.orandr.movieme.toolbar.Frag2_upcoming.PROPERTIE_MOVIE;

public class PropertieDetailsActivity extends AppCompatActivity {

    private final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    private final String LANGUAGE = "fr-FR";
    private static final String TAG = "PropertieDetailsActivit";
    private static final int TEMPS_REFRESH = 1000;
    private TextView title;
    private ImageView poster_path;
    private TextView overview;
    private TextView vote_average;
    private TextView release_date;
    private TextView runtime;
    private TextView genres;
    private TextView adult;
    private TextView popularity;
    private TextView realisateur;
    private Properties properties;
    private Button addFavoriteImage;
    private Button removeFavoriteImage;
    private FavoritesFragment favoritesFragment;
    RecyclerView recyclerView_similaire;
    RecyclerViewAdapter_details recyclerViewAdapter_details_similaire;
    RecyclerView recyclerView_acteur;
    RecyclerViewAdapter_details_acteur recyclerViewAdapter_details_acteur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propertie_details);

        release_date = findViewById(R.id.propertie_release_date);
        runtime = findViewById(R.id.view_duree);
        genres = findViewById(R.id.view_genre);
        adult = findViewById(R.id.view_adulte);
        popularity = findViewById(R.id.view_popularity);

        title = findViewById(R.id.movie_title);
        poster_path = findViewById(R.id.poster_path_image);
        overview = findViewById(R.id.movie_overview);
        vote_average = findViewById(R.id.propertie_vote_average);

        realisateur = findViewById(R.id.view_realisateur);

        addFavoriteImage = findViewById(R.id.add_favorite_button);
        removeFavoriteImage = findViewById(R.id.remove_favorite_button);

        if(getIntent().hasExtra(PROPERTIE_MOVIE)) {
            Parcelable parcelable = getIntent().getParcelableExtra(PROPERTIE_MOVIE);
            properties = new Properties();
            properties = Parcels.unwrap(parcelable);
//            displayData(properties);
        }


        favoritesFragment = new FavoritesFragment(getApplicationContext());
        content_refresh();

        addFavoriteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoritesFragment.addList(properties);
                content_refresh();
            }
        });


        removeFavoriteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoritesFragment.removeList(favoritesFragment.getRecyclerAdapter(), properties);
                content_refresh();
            }
        });



        FetchMovie fetchMovie = new FetchMovie(getApplicationContext());

        WebView webView;
        webView = findViewById(R.id.video_play);


        recyclerView_similaire = findViewById(R.id.recyclerViewAdapter_details_similaire);

//        recyclerView_similaire.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView_similaire.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView_similaire.setItemAnimator(new DefaultItemAnimator());

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView_similaire.getContext(), LinearLayout.VERTICAL);

        recyclerView_similaire.addItemDecoration(dividerItemDecoration);

        recyclerViewAdapter_details_similaire = new RecyclerViewAdapter_details(new ArrayList(), new RecyclerViewAdapter_details.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Properties properties) {
                Intent intent = new Intent(getApplicationContext(), PropertieDetailsActivity.class);
                Parcelable parcelable = Parcels.wrap(properties);
                intent.putExtra(PROPERTIE_MOVIE, parcelable);
                startActivity(intent);
            }
        }, getApplicationContext());

        recyclerView_similaire.setAdapter(recyclerViewAdapter_details_similaire);


        recyclerView_acteur = findViewById(R.id.recyclerViewAdapter_details_acteur);

        recyclerView_acteur.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView_acteur.setItemAnimator(new DefaultItemAnimator());

        DividerItemDecoration dividerItemDecoration_acteur = new DividerItemDecoration(recyclerView_acteur.getContext(), LinearLayout.VERTICAL);

        recyclerView_acteur.addItemDecoration(dividerItemDecoration_acteur);

        recyclerViewAdapter_details_acteur = new RecyclerViewAdapter_details_acteur(new ArrayList(), getApplicationContext());

        recyclerView_acteur.setAdapter(recyclerViewAdapter_details_acteur);


        fetchMovie.fetchMovieData_Similar_recycler(properties.getId(), recyclerViewAdapter_details_similaire);

        fetchMovie.fetchMovieData_Video(webView, properties.getId());
        fetchMovie.fetchMovieData_Movie(realisateur, properties.getId(), recyclerViewAdapter_details_acteur);
        fetchMovie.fetchMovieData_Detail(title, poster_path, release_date, runtime, genres, adult, popularity, overview, vote_average, properties.getId());

    }

    public void content_refresh() {
        if(favoritesFragment.noContain(properties)) {
            addFavoriteImage.setVisibility(View.VISIBLE);
            removeFavoriteImage.setVisibility(View.INVISIBLE);
        } else {
            addFavoriteImage.setVisibility(View.INVISIBLE);
            removeFavoriteImage.setVisibility(View.VISIBLE);
        }
        refresh();
    }


    public void refresh() {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                content_refresh();
            }
        };

        handler.postDelayed(runnable, TEMPS_REFRESH);
    }

}