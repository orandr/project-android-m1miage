package com.orandr.movieme.toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.orandr.movieme.PropertieDetailsActivity;
import com.orandr.movieme.R;
import com.orandr.movieme.api.FetchMovie;
import com.orandr.movieme.api.MovieService;
import com.orandr.movieme.api.RetrofitClient;
import com.orandr.movieme.model.ListCollection;
import com.orandr.movieme.model.Properties;
import com.orandr.movieme.recycler.RecyclerViewAdapter;
import org.parceler.Parcels;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Frag4_top_rated extends Fragment {

    Context context;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerAdapter;
    public static final String PROPERTIE_MOVIE = "propertie_movie";
    private final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    private static final String TAG = "Frag2_upcoming";
    private final String LANGUAGE = "fr-FR";
    Frag4_top_rated top_rated;
    ImageButton button_precedent;
    ImageButton button_suivant;
    TextView totalPage;
    TextView nowPage;
    public int numeroPageNow;
    int nbTotalPage;
    private static final int TEMPS_REFRESH = 1000;


    public Frag4_top_rated(Context context) {
        this.context = context;
        this.numeroPageNow = 1;
        this.nbTotalPage = 354;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag4_top_rated, container, false);

        top_rated = new Frag4_top_rated(context);

        button_precedent = view.findViewById(R.id.imageButton_precedent);
        button_suivant = view.findViewById(R.id.imageButton_suivant);
        totalPage = view.findViewById(R.id.text_pageTotal);
        nowPage = view.findViewById(R.id.text_pageNow);


        recyclerView = view.findViewById(R.id.recyclerViewFrag4);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);

        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Properties properties) {
                Intent intent = new Intent(context, PropertieDetailsActivity.class);
                Parcelable parcelable = Parcels.wrap(properties);
                intent.putExtra(PROPERTIE_MOVIE, parcelable);
                startActivity(intent);
            }
        }, context);


        button_precedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                top_rated.precedent(nowPage, button_precedent, button_suivant, recyclerAdapter);
            }
        });

        button_suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                top_rated.suivant(nowPage, button_precedent, button_suivant, recyclerAdapter);
            }
        });

        button_precedent.setVisibility(View.INVISIBLE);

        this.nowPage.setText(""+this.numeroPageNow);

        this.fetchMovieData_TopRated_page(recyclerAdapter, numeroPageNow);


        totalPage.setText("/ "+this.nbTotalPage);


        recyclerView.setAdapter(recyclerAdapter);


        return view;
    }

    public void fetchMovieData_TopRated_page(final RecyclerViewAdapter recyclerAdapter, int num_page) {
        MovieService movieService = RetrofitClient.getInstance().create(MovieService.class);
        movieService.getTopRatedPage(API_KEY, LANGUAGE, num_page)
                .enqueue(new Callback<ListCollection>() {
                    @Override
                    public void onResponse(Call<ListCollection> call, Response<ListCollection> response) {
                        if( response.isSuccessful() && response.body() != null ) {
                            //Manage Data
                            recyclerAdapter.addFeatureList(response.body().getResult());
                            nbTotalPage = response.body().getTotal_pages();
                        } else {
                            Toast.makeText(context, "Erreur, fetchMovieData_TopRated", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override public void onFailure(Call<ListCollection> call, Throwable t) {
                        //Manage errors
                    }
                });
    }


    public void content_refresh(TextView page_now, ImageButton btn_prec, ImageButton btn_suiv) {


        if(  this.nbTotalPage <= this.numeroPageNow + 1) {
            btn_suiv.setVisibility(View.INVISIBLE);
        } else {
            btn_suiv.setVisibility(View.VISIBLE);
        }



        if(this.numeroPageNow - 1 <= 0 ) {
            btn_prec.setVisibility(View.INVISIBLE);
        } else {
            btn_prec.setVisibility(View.VISIBLE);
        }


        page_now.setText(""+this.numeroPageNow);
        refresh(page_now, btn_prec, btn_suiv);
    }


    public void refresh(final TextView page_now, final ImageButton btn_prec, final ImageButton btn_suiv) {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                content_refresh(page_now, btn_prec, btn_suiv);
            }
        };

        handler.postDelayed(runnable, TEMPS_REFRESH);
    }



    public void suivant(TextView page_now, ImageButton btn_prec, ImageButton btn_suiv, RecyclerViewAdapter recyclerViewAdapter){
        if(this.numeroPageNow + 1 < nbTotalPage) {
            this.numeroPageNow++;
            this.fetchMovieData_TopRated_page(recyclerViewAdapter, this.numeroPageNow);
            content_refresh(page_now, btn_prec, btn_suiv);
        }
    }




    public void precedent(TextView page_now, ImageButton btn_prec, ImageButton btn_suiv,  RecyclerViewAdapter recyclerViewAdapter){
        if(0 < this.numeroPageNow - 1) {
            this.numeroPageNow--;
            this.fetchMovieData_TopRated_page(recyclerViewAdapter, this.numeroPageNow);
            content_refresh(page_now, btn_prec, btn_suiv);
        }
    }

}
