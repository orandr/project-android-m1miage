
package com.orandr.movieme.model;

import java.util.ArrayList;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Properties$$Parcelable
    implements Parcelable, ParcelWrapper<com.orandr.movieme.model.Properties>
{

    private com.orandr.movieme.model.Properties properties$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<Properties$$Parcelable>CREATOR = new Creator<Properties$$Parcelable>() {


        @Override
        public Properties$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new Properties$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public Properties$$Parcelable[] newArray(int size) {
            return new Properties$$Parcelable[size] ;
        }

    }
    ;

    public Properties$$Parcelable(com.orandr.movieme.model.Properties properties$$2) {
        properties$$0 = properties$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(properties$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(com.orandr.movieme.model.Properties properties$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(properties$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(properties$$1));
            parcel$$1 .writeString(properties$$1 .overview);
            parcel$$1 .writeString(properties$$1 .original_language);
            parcel$$1 .writeString(properties$$1 .original_title);
            parcel$$1 .writeInt((properties$$1 .video? 1 : 0));
            parcel$$1 .writeString(properties$$1 .title);
            if (properties$$1 .genre_ids == null) {
                parcel$$1 .writeInt(-1);
            } else {
                parcel$$1 .writeInt(properties$$1 .genre_ids.size());
                for (java.lang.Integer integer$$0 : properties$$1 .genre_ids) {
                    if (integer$$0 == null) {
                        parcel$$1 .writeInt(-1);
                    } else {
                        parcel$$1 .writeInt(1);
                        parcel$$1 .writeInt(integer$$0);
                    }
                }
            }
            parcel$$1 .writeString(properties$$1 .poster_path);
            parcel$$1 .writeString(properties$$1 .backdrop_path);
            parcel$$1 .writeString(properties$$1 .release_date);
            parcel$$1 .writeFloat(properties$$1 .popularity);
            parcel$$1 .writeFloat(properties$$1 .vote_average);
            parcel$$1 .writeInt(properties$$1 .id);
            parcel$$1 .writeInt((properties$$1 .adult? 1 : 0));
            parcel$$1 .writeInt(properties$$1 .vote_count);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.orandr.movieme.model.Properties getParcel() {
        return properties$$0;
    }

    public static com.orandr.movieme.model.Properties read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            com.orandr.movieme.model.Properties properties$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            properties$$4 = new com.orandr.movieme.model.Properties();
            identityMap$$1 .put(reservation$$0, properties$$4);
            properties$$4 .overview = parcel$$3 .readString();
            properties$$4 .original_language = parcel$$3 .readString();
            properties$$4 .original_title = parcel$$3 .readString();
            properties$$4 .video = (parcel$$3 .readInt() == 1);
            properties$$4 .title = parcel$$3 .readString();
            int int$$0 = parcel$$3 .readInt();
            ArrayList<java.lang.Integer> list$$0;
            if (int$$0 < 0) {
                list$$0 = null;
            } else {
                list$$0 = new ArrayList<java.lang.Integer>(int$$0);
                for (int int$$1 = 0; (int$$1 <int$$0); int$$1 ++) {
                    int int$$2 = parcel$$3 .readInt();
                    java.lang.Integer integer$$1;
                    if (int$$2 < 0) {
                        integer$$1 = null;
                    } else {
                        integer$$1 = parcel$$3 .readInt();
                    }
                    list$$0 .add(integer$$1);
                }
            }
            properties$$4 .genre_ids = list$$0;
            properties$$4 .poster_path = parcel$$3 .readString();
            properties$$4 .backdrop_path = parcel$$3 .readString();
            properties$$4 .release_date = parcel$$3 .readString();
            properties$$4 .popularity = parcel$$3 .readFloat();
            properties$$4 .vote_average = parcel$$3 .readFloat();
            properties$$4 .id = parcel$$3 .readInt();
            properties$$4 .adult = (parcel$$3 .readInt() == 1);
            properties$$4 .vote_count = parcel$$3 .readInt();
            com.orandr.movieme.model.Properties properties$$3 = properties$$4;
            identityMap$$1 .put(identity$$1, properties$$3);
            return properties$$3;
        }
    }

}
